# spring-boot-web-test
Import spring boot web service sample code from https://github.com/blackOcean/spring-boot-web-service

### Technology Stack
1. Spring Boot
2. Hibernate with JPA 
3. In memory database h2
4. Spring boot starter test


#### Testing Scopes : 
* Unit testing of methods
* Layer testing (controller, service and DAO)
* Integration testing (Complete workflow -> Controller to DAO layer)
 

**What is Spring boot starter test ?** 

Its a framework which contains all the dependencies which we need to test spring boot applications.
* Junit
* Mockito
* AssertJ