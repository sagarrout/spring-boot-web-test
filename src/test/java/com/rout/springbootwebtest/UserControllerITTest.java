package com.rout.springbootwebtest;

import com.rout.springbootwebtest.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerITTest {

    @Autowired
    TestRestTemplate restTemplate;

    @Test
    public void test(){

        ResponseEntity<User> response = restTemplate.getForEntity("/users/{id}",User.class,1L);
        assertThat(response.getStatusCodeValue()).isEqualTo(500);
    }
}
