package com.rout.springbootwebtest.service;

import com.rout.springbootwebtest.dao.UserDao;
import com.rout.springbootwebtest.domain.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.*;

public class UserServiceTest {

    @Rule // For exception
    public ExpectedException thrown = ExpectedException.none();


    // Mock -> Mockito is creating instance for you
    @Mock // Could use @Spy also just to check the call to that layer
    UserDao userDao;

    private UserService userService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this); // mock triggering
        this.userService = new UserServiceImpl(userDao);
    }


    // Method name -> testing method name action perform and expected result
    @Test
    public void getShouldReturnUser(){

        User user = new User(1L,"Sagar","Rout","0123456789");

        // Stubbing
        BDDMockito.given(userDao.getOne(Mockito.anyLong())).willReturn(user);

        assertThat(this.userService.get(1L)).isEqualTo(user);
    }


    @Test
    public void getUserWhenUserNotPresentShouldThrowException() throws Exception{
        BDDMockito.given(userDao.getOne(Mockito.anyLong())).willReturn(null);

        thrown.expect(IllegalArgumentException.class);

        userService.get(1L);
    }
}