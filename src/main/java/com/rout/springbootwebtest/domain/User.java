package com.rout.springbootwebtest.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "USERS")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "PHONE_NO")
    private String cellNo;

    public User() {
    }

    public User(Long id, String name, String lastName, String cellNo) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.cellNo = cellNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCellNo() {
        return cellNo;
    }

    public void setCellNo(String cellNo) {
        this.cellNo = cellNo;
    }
}
