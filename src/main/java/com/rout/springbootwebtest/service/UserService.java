package com.rout.springbootwebtest.service;

import com.rout.springbootwebtest.domain.User;

public interface UserService {

    User get(Long id);

    User saveOrUpdate(User user);

    void delete(Long id);
}
