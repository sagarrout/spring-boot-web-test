package com.rout.springbootwebtest.service;

import com.rout.springbootwebtest.dao.UserDao;
import com.rout.springbootwebtest.domain.User;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User get(Long id) {
        User user = userDao.getOne(id);

        if(user!=null)
            return user;

        throw new IllegalArgumentException();
    }

    @Override
    public User saveOrUpdate(User user) {
        return userDao.save(user);
    }

    @Override
    public void delete(Long id) {
        userDao.delete(id);
    }
}
