package com.rout.springbootwebtest.dao;

import com.rout.springbootwebtest.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Long> {

}
