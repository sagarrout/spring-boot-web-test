package com.rout.springbootwebtest.controller;

import com.rout.springbootwebtest.domain.User;
import com.rout.springbootwebtest.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    // Best practice
    private final UserService userService;

    // Always use constructor injection
    // Note : Ease to inject in testing
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> get(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.userService.get(id));
    }

    @PostMapping(path = "/users", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> post(@RequestBody User user) {
        return ResponseEntity.ok(this.userService.saveOrUpdate(user));
    }

    @DeleteMapping(path = "/users/{id}")
    public void delete(@PathVariable("id") Long id) {
        this.userService.delete(id);
    }

}
